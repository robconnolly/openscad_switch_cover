
# Home Assistant Themed Parametric Light Switch Cover

A Home Assistant themed parametric light switch cover designed with OpenSCAD.

See the blog post for details:
[https://webworxshop.com/solving-smart-bulb-problems-with-3d-printing/](https://webworxshop.com/solving-smart-bulb-problems-with-3d-printing/?pk_campaign=gitlab)

You can also grab the files from Thingiverse:
[https://www.thingiverse.com/thing:4000020](https://www.thingiverse.com/thing:4000020)

