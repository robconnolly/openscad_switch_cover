
// Thickness of the wall
wall_thickness = 1;
// Length of the button
button_length = 25;
// Width of the button
button_width = 50;
// Height of the button
button_height = 8;
// X offset of the logo
logo_x_offset = 12.5;
// Y offset of the logo
logo_y_offset = 0;
// Width of the tabs
tab_width = 10;
// Height/thickness of the tabs
tab_height = 2;

module tapered_box(points, radius, height){
    hull(){
        for (p = points){
            translate(p) cylinder(r=radius, r2=0, h=height);
        }
    }
}

module hollow_cover(side_length, side_width, wall_thickness, height)
{
    outer_points = [
        [0,0,0],
        [side_length,0,0],
        [0,side_width,0],
        [side_length,side_width,0]
    ];
    inner_length = side_length-wall_thickness;
    inner_width = side_width-wall_thickness;
    inner_points = [
        [wall_thickness,wall_thickness,-wall_thickness],
        [inner_length,wall_thickness,-wall_thickness],
        [wall_thickness,inner_width,-wall_thickness],
        [inner_length,inner_width,-wall_thickness]
    ];

    difference()
    {
        tapered_box(outer_points, 2, height);
        tapered_box(inner_points, 2, height-2);
    }
}

difference()
{
    rotate([0, 0, 90])
    {
        difference()
        {
            union()
            {
                hollow_cover(button_length, button_width, wall_thickness, button_height);
                translate([0, button_width+wall_thickness, 0])
                    cube([button_length, tab_width, tab_height]);
                translate([0, -(tab_width+wall_thickness), 0])
                    cube([button_length, tab_width, tab_height]);
            }

            rotate([0, 0, 0])
            {
                translate([button_length+logo_y_offset+1, 0, button_height+0.1])
                {
                    rotate([0, 0, 0])
                    {
                        scale([button_length / 128, button_length / 128, -1.1 / 256])
                            mirror([1, 0, 0]) {
                                surface(file = "logo-small.dat", convexity = 5);
                            }
                    }
                }
            }
        }
    }
    
    translate([-(button_length+1.25*tab_width), -3, 0]) {
        cube([button_length + 2.5*tab_width, 5, button_height]);
    }
}