width = 128 # => width of resized raw image
str = File.binread('logo-small.raw')
pixels = str.unpack("C*")

File.open('logo-small.dat', 'w') do |f|
    pixels.each_with_index do |pixel, idx|
        f.write(pixel)
        if ((idx + 1) % width) == 0
            f.write("\n")
        else
            f.write(" ")
        end
    end
    f.write("\n")
end
